require 'sinatra'
require_relative 'lib/chemoSummarizer'
require 'sqlite3'
require 'date'
require 'nokogiri'
require_relative 'NotFound'


$result = ''

def generate_summary(name,key,inchi,access)

  not_found = 0
  # Flag that indicates if HTML should be updated based on Date or if the record
  # in the database is empty.
  update_record = 0

  # Flag for exists variable
  exists = nil

  pub_chem_id = nil

  record_exists = nil
  # The process below allows a name to be searched and get the top result from pubchem to ensure whatever term/synonym is used
  # to find a compund that it is linked with the right record from the database.

  # Change name to proper fromat for pubchem Search
  #name.to_s
  #data = "\"#{name}\""

  # Send query to search page in PubChem to find most relevant compund based on its name.
  # pubchem_search_results = Nokogiri::HTML(open("https://www.ncbi.nlm.nih.gov/pccompound?term=#{data}"))

  # Extract the link of the most relevant result
  #search = pubchem_search_results.css('p.title>a')[0]['href']


  # Send query to most relevant compound page and extract PubChem_CID from the page to ensure all names of the same compound
  # can extract the same record of the database.
  #top_result = Nokogiri::HTML(open("https:#{search}"))
  #pub_chem_id = top_result.css('head').at('meta[name = pubchem_uid_value]')['content'].to_i

  #puts pub_chem_id
  # Opens database
  db = SQLite3::Database.new '/home/mitchell/PycharmProjects/Sdf_parser/test.db'

  if name.present?
    # lower cases the name as the IUPAC names in database all begin with lowercase letters
    # Might want to adjust in the future...

    # Duplicates name to avoid causing name issues with main program
    # this allow the alteration of the name for a web search if it contains a space
    # Also to avoid the case where subbing and changing the main name without the
    # existance of whitespace messing with later functions.

    name_dup = name.dup
    if name_dup.match(/\s/)
      name_dup.gsub!(' ', '%20')
    end

    # CHECK FOR IF IUPAC THEN RETRIEVE ITEM FROM DATABASE WITH IT, ELSE USE THE PUG/RESTFUL API
    # TO OBTAIN PUBCHEM CID TO USE FOR FETTCHING.

    # Restful generally the most accurate method of determining the correct name but can be wrong
    # when compound spelled incorrectly

    #search_term = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/#{name_dup}/cids/XML"

    #pubchem_search_results = Nokogiri::XML(open(search_term))

    # puts name_dup
    pub_chem_id =  NotFound.new(name_dup, 1).search_result.to_i
    if pub_chem_id != nil
      exists = 1
    end



    # Changes the entered name from uppercase to lowercase if it is as the database is searched by
    # a capital lettered name
    #lowercase_name = name
    #lowercase_name[0] = lowercase_name[0].downcase

    #update_record = 0
    # Polls the database to see if the IUPAC name is in it

    #exists = db.execute('select PUBCHEM_CID from PubChem_Ref WHERE PUBCHEM_IUPAC = ?', lowercase_name)

    #name[0] = name[0].upcase

    # if it returns a value, then check if HTML file is not 'nil' or out of date by 6 months otherwise
    # it will call to DataWrangler to scrap and then using that generated template store it into the
    # database for future use.
    if exists

        results = db.execute('SELECT HTML, CREATION_DATE FROM HTML_BLOB WHERE PUBCHEM_CID = ?', pub_chem_id)

        #puts '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        current_date = DateTime.current.to_date

        if results == []

          update_record = 1
          compound = ChemoSummarizer::get_by_name(name)

        else
          record_exists = true
          if results[0][1] != nil
            date_diff = Date.parse("#{current_date}") - Date.parse("#{results[0][1]}")
          end

          if results[0][0] == 'nil' or date_diff > 167
            #print 'IM BEING UPDATED'
            update_record = 1
            #print 'IM THE NAME THAT DOESN"T WORK'
            #print name
            compound = ChemoSummarizer::get_by_name(name)

          else
            return results[0][0]

          end

        end

    else
      compound = ChemoSummarizer::get_by_name(name)
    end

  elsif key.present?

    pub_chem_id =  NotFound.new(key, 1).search_result.to_i
    if pub_chem_id != nil
      exists = 1
    end

    if exists
      results = db.execute('SELECT HTML, CREATION_DATE FROM HTML_BLOB WHERE PUBCHEM_CID = ?', pub_chem_id)
      if results != []

        return results[0][0]
      else
        compound = ChemoSummarizer::get_by_inchikey(key)
      end
    else
      compound = ChemoSummarizer::get_by_inchikey(key)
    end

  elsif inchi.present?
    pub_chem_id =  NotFound.new(inchi, 1).search_result.to_i
    if pub_chem_id != nil
      exists = 1
    end

    if exists
      results = db.execute('SELECT HTML, CREATION_DATE FROM HTML_BLOB WHERE PUBCHEM_CID = ?', pub_chem_id)
      if results != []

        return results[0][0]
      else
        compound = ChemoSummarizer::get_by_inchikey(inchi)
      end
    else
      compound = ChemoSummarizer::get_by_inchikey(inchi)
    end
  end

  result = ChemoSummarizer.summarize_for_app(compound)
  # Check if resutl is null, If it is make a record for it and generate data based on its pubchem id and info
  # Store barebones HTML Template and Gsub in images, parameters, from classyfire and pubchem etc.

  #puts result.to_s == '<div><h1 style = "word-break:break-all">Compound not found</h1></div>'

  if result.to_s == '<div><h1 style = "word-break:break-all">Compound not found</h1></div>'
    not_found = 1

    if name
      results = NotFound.new(name, 1)
      $result = results.final_result

      curr_date = DateTime.current.to_date.to_s
      if record_exists
        db.execute('UPDATE HTML_BLOB SET HTML = ?, CREATION_DATE = ? WHERE PUBCHEM_CID = ?', $result, curr_date, pub_chem_id)
      else
        db.execute('INSERT INTO HTML_BLOB (PUBCHEM_CID, HTML, CREATION_DATE) VALUES (?, ?, ?) ', pub_chem_id, $result, curr_date)
      end



    if inchi
      results = NotFound.new(inchi, 2)
      $result = results.final_result

      curr_date = DateTime.current.to_date.to_s
      if record_exists
        db.execute('UPDATE HTML_BLOB SET HTML = ?, CREATION_DATE = ? WHERE PUBCHEM_CID = ?', $result, curr_date, pub_chem_id)
      else
        db.execute('INSERT INTO HTML_BLOB (PUBCHEM_CID, HTML, CREATION_DATE) VALUES (?, ?, ?) ', pub_chem_id, $result, curr_date)
      end
    end


    if key
      results = NotFound.new(key, 3)
      $result = results.final_result

      curr_date = DateTime.current.to_date.to_s
      if record_exists
        db.execute('UPDATE HTML_BLOB SET HTML = ?, CREATION_DATE = ? WHERE PUBCHEM_CID = ?', $result, curr_date, pub_chem_id)
      else
        db.execute('INSERT INTO HTML_BLOB (PUBCHEM_CID, HTML, CREATION_DATE) VALUES (?, ?, ?) ', pub_chem_id, $result, curr_date)
      end
    end

    end

  end





  #Capture SQL BLOB of HTML file here if it doesn't exist in the HTML BLOB tables.
  if update_record == 1 and not_found == 0
    curr_date = DateTime.current.to_date.to_s
    if record_exists == nil
    #puts curr_date
      db.execute('INSERT INTO HTML_BLOB (PUBCHEM_CID, HTML, CREATION_DATE) VALUES (?, ?, ?) ', pub_chem_id, result, curr_date)
    else
      db.execute('UPDATE HTML_BLOB SET HTML = ?, CREATION_DATE = ? WHERE PUBCHEM_CID = ?', result, curr_date, pub_chem_id)
    end

    #ActiveRecord::Base.connection.execute("BEGIN TRANSACTION; END;")

    return result
  end

  if not_found == 0
    return result
  else
    return $result
  end


end

get "/" do erb :home
end



get "/search/" do
  @name = params[:name]
  @key  = params[:key]
  @inchi = params[:inchi]
  @access = params[:access]
	if :name.nil?
		return 'Parameter missing error'
	else
		@html = generate_summary(@name,@key,@inchi,@access)

		erb :result
	end
end
