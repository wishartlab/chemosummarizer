require 'chemspider'
require 'nokogiri'

query = ''

class NotFound
  def initialize(data, type)
    # To sort the data properly, @data is either a name, inchi or inchikey,
    # with @type being 1 , 2 or 3 to identify each identifier/search term
    @data = data
    @type = type

    #String to be placed into HTML page
    @property_string = ''
    @image_src = ''
    @desc_string = ''

    #Main Identifiers section
    @classysmiles = nil
    @spider_inchi = nil
    @inchikey = nil
    @chemspiderid = nil
    @spider_iupac = nil

    #Properties section
    @state = nil
    @name = ''
    @chemical_formula = ''
    @boiling_point = nil
    @polarizability = ''
    @chem_spider_mol_weight = 0
    @formal_charge = 0
    @rotatable_bonds = 0
    @donors = 0
    @acceptors = 0
    @logP = ''
    @density = ''
    @polar_surface_area = 0

    #Classyfire material

    @kingdom = ''
    @superclass = ''
    @class = ''
    @direct_parent = ''
    @classy_desc = nil

    #pubchem section/material
    @pubchem_id = nil

    #chemspider
    @spider_image = nil


  end
  def search_result
    chemspider_parser
    description

  end

  def final_result
    chemspider_parser
    description
    write_sentence1
    write_polarizability
    #write_formal_charge
    write_Lipinski
    write_Veber
    html
  end


  def chemspider_parser
    query = @data
    token = '8fed644c-d0c2-4b01-abd5-55678b69a87f'

# the maximum number of attempts...
    request_count = 10

# the amount of time to "sleep" between requests...
    cooloff = 1.0

# $stderr.puts("[Search] #{query.inspect}")

# send the request, and receive the transaction ID...
    rid = ChemSpider::Search::AsyncSimpleSearch.get!(:query => query, :token => token)

    result_ready = false

    (0..request_count).each do |idx|
      # request the current status for the transaction ID...
      status = ChemSpider::Search::GetAsyncSearchStatus.get!(:rid => rid, :token => token)

      # $stderr.puts("[Status #{idx.to_i + 1} of #{ASYNC_SEARCH_STATUS_REQUEST_COUNT}] #{rid} => #{status}")

      case status
        when "ResultReady"
          # toggle the flag...
          result_ready = true

          # we're ready to go...
          break
        when "Failed", "Suspended", "TooManyRecords"
          # the search has failed (for some reason)...
          break
        else
          # wait...
          Kernel.sleep(cooloff)
      end
    end

    if result_ready
      # request the results and get the Chemspider ID
      csids = ChemSpider::Search::GetAsyncSearchResult.get!(:rid => rid, :token => token)

      if csids == []
        # Send query to search page in PubChem to find most relevant compund based on its name.
        # puts @data
        pubchem_search_results = Nokogiri::HTML(open("https://www.ncbi.nlm.nih.gov/pccompound?term=#{@data}"))



        begin
          # Exceptions raised by this code will
          # be caught by the following rescue clause

          # Extract the link of the most relevant result
          search = pubchem_search_results.css('p.title>a')[0]['href']
          # Send query to most relevant compound page and extract PubChem_CID from the page to ensure all names of the same compound
          # can extract the same record of the database.
          top_result = Nokogiri::HTML(open("https:#{search}"))
          @pubchem_id = top_result.css('head').at('meta[name = pubchem_uid_value]')['content'].to_i

          # puts @pubchem_id

        rescue NoMethodError
          @pubchem_id = pubchem_search_results.css('head').at('meta[name = pubchem_uid_value]')['content'].to_i
        end

        search_term = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/#{@pubchem_id}/property/InChIKey/XML"

        # puts search_term

        # puts @pubchem_id

        pubchemresults = Nokogiri::XML(open(search_term))

        @inchikey = pubchemresults.css('InChIKey').text

        # puts @inchikey
        # puts 'After Pubchem'


        #TRY search again

        rid = ChemSpider::Search::AsyncSimpleSearch.get!(:query => @inchikey, :token => token)

        result_ready = false

        (0..request_count).each do |idx|
          # request the current status for the transaction ID...
          status = ChemSpider::Search::GetAsyncSearchStatus.get!(:rid => rid, :token => token)

          # $stderr.puts("[Status #{idx.to_i + 1} of #{ASYNC_SEARCH_STATUS_REQUEST_COUNT}] #{rid} => #{status}")

          case status
            when "ResultReady"
              # toggle the flag...
              result_ready = true
              csids = ChemSpider::Search::GetAsyncSearchResult.get!(:rid => rid, :token => token)

              # we're ready to go...
              break
            when "Failed", "Suspended", "TooManyRecords"
              # the search has failed (for some reason)...
              break
            else
              # wait...
              Kernel.sleep(cooloff)
          end
        end
      end

      extended_compound_info = ChemSpider::MassSpecAPI::GetExtendedCompoundInfo.get!(:CSID => csids[0], :token => token)

      @chemspiderid = csids[0].to_i
      chemspider_search_results = Nokogiri::HTML(open("http://www.chemspider.com/Chemical-Structure.#{@chemspiderid}.html"))


      predicted_html = chemspider_search_results.css("div.AspNet-FormView-Data").css('tr')

      #Grabs the inchikey from spider page

      if @inchikey == nil
        std_inchikey_first = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_moreDetails_StdInChIKey_conn"]').text
        std_inchikey_second = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_moreDetails_StdInChIKey_rest"]').text
        @inchikey = std_inchikey_first + '-' +std_inchikey_second
      end

      #Grabs the inchi from spider page
      @spider_image = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_ThumbnailControl1_viewMolecule"]').attr('src')


      @spider_inchi = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_moreDetails_WrapControl4"]').text

      #Grabs the molecular formula from spider page
      @chemical_formula = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_prop_MF"]').text

      #grabs IUPAC
      @spider_iupac = chemspider_search_results.css('[id = "ctl00_ctl00_ContentSection_ContentPlaceHolder1_RecordViewDetails_rptDetailsView_ctl00_moreDetails_WrapSysName"]').text

      #grabs chemspider molecular weight
      @chem_spider_mol_weight = extended_compound_info.instance_variable_get(:@molecular_weight)

      predicted_titles = predicted_html.css('td.prop_title')
      predicted_values = predicted_html.css('td.prop_value_nowrap')


      index = 0
      predicted_titles.each { |title| if title.text.gsub(/\s+/, "") == 'Density:'
                                        @density = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == 'BoilingPoint:'
                                        @boiling_point = predicted_values[index].text.gsub('Â', '')
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == '#Hbondacceptors:'
                                        @acceptors = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == '#Hbonddonors:'
                                        @donors = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == '#FreelyRotatingBonds:'
                                        @rotatable_bonds = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == 'ACD/LogP:'
                                        @logP = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == 'PolarSurfaceArea:'
                                        @polar_surface_area = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      elsif title.text.gsub(/\s+/, "") == 'Polarizability:'
                                        @polarizability = predicted_values[index].text.gsub('Â', '').gsub(/\s+/, "")
                                        index+=1
                                        next
                                      else

                                        index+=1
                                      end
      }

    end

    end
  end


  def description
    #classyfire section

    begin
      # Exceptions raised by this code will
      # be caught by the following rescue clause
      @classyfire_URL = Nokogiri::HTML(open "http://classyfire.wishartlab.com/entities/#{@inchikey}/")

      @image_src =  @classyfire_URL.css('img')[0]['src']
      classydata = @classyfire_URL.css("p.text_card")

      @classysmiles = classydata[2].text

      index = 0

      classydata.each { |name| if name.text == 'Description'
                                 @classy_desc = classydata[index+1].text
                                 index+=1
                                 next
                               elsif name.text == 'Kingdom'
                                 @kingdom = classydata[index+1].text
                                 index+=1
                                 next
                               elsif name.text == 'Superclass'
                                 @superclass = classydata[index+1].text
                                 index+=1
                                 next
                               elsif name.text == 'Class'
                                 @class = classydata[index+1].text
                                 index+=1
                                 next
                               elsif name.text == 'Direct Parent'
                                 @direct_parent = classydata[index+1].text
                                 index+=1
                                 next
                               else
                                 index+=1

                               end}


    rescue OpenURI::HTTPError
      # puts "Classyfire not found"
      @image_src = @spider_image

    end



    #pubchem section
    if @pubchem_id == nil

      search_term = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/inchikey/#{@inchikey}/cids/XML"
      pubchemres = Nokogiri::XML(open(search_term))
      @pubchem_id = pubchemres.css('CID').text.to_i
    end


    pubchem_search_results = Nokogiri::HTML(open("https://www.ncbi.nlm.nih.gov/pccompound?term=#{@inchikey}"))



    #pub_chem_name = pubchem_search_results.css('head').at('meta[name = description]')['content'].split('|')

    #print pub_chem_name
    head = pubchem_search_results.css('head')
    descriptionsec = head.at('meta[name = description]')['content']

    # print descriptionsec

    name = descriptionsec.split('|')[0]

    @name = name

    if @classy_desc
      @desc_string += @classy_desc
    end

    @pubchem_id

  end





  def write_sentence1
    @property_string += "Also known as #{@data}, " if @type == 1
    @property_string += "Also known as #{@name}, " if @type != 1
    unless @state.nil? || @state == ''
      @property_string +=  "#{@name} exists as a #{@state.downcase} compound"
    end
    unless @boiling_point.nil?
      if @state.nil?
        @property_string  += "#{@name} has a boiling point of #{@boiling_point}"
      else
        @property_string += " which has a boiling point of #{@boiling_point}"
      end
    end
    unless @chemical_formula.nil?
      if @boiling_point.nil? && @state.nil?
        @property_string += "#{@name} has a chemical formula of #{@chemical_formula} "
      else
        if @boiling_point.nil? || @state.nil?
          @property_string += " with a chemical formula of #{@chemical_formula} "
        else
          @property_string += ", it also has a chemical formula of #{@chemical_formula} "
        end
      end
    end

    unless @chem_spider_mol_weight.nil?
      if @state.nil? &&  @boiling_point.nil? && @chemical_formula.nil?
        @property_string += "#{@name} has an average mass of #{@chem_spider_mol_weight.to_f.round(3)}. "
      else
        if @chemical_formula.nil?
          @property_string += "and also having an average mass of #{@chem_spider_mol_weight.to_f.round(3)}. "
        else
          if @boiling_point.nil?
            @property_string += "and has an average mass of #{@chem_spider_mol_weight.to_f.round(3)}. "
          else
            @property_string += " with an average mass of #{@chem_spider_mol_weight.to_f.round(3)}. "
          end

        end
      end
    end
  end

  def write_polarizability
    unless @polarizability.nil?
      polarizability = @polarizability.to_f.round(3)

      avg_pol = 99.40047619 #Number based off average of Lippincot Method (Theoretical) from this paper http://nopr.niscair.res.in/bitstream/123456789/26124/1/IJPAP%2042(6)%20407-410.pdf

      average = "<a href='http://nopr.niscair.res.in/bitstream/123456789/26124/1/IJPAP%2042(6)%20407-410.pdf' title='Lippincot Method Average'>Lippincot Method Average</a>"

      pol_ref = "<a href='https://chem.libretexts.org/Core/Physical_and_Theoretical_Chemistry/Physical_Properties_of_Matter/Atomic_and_Molecular_Properties/Intermolecular_Forces/Specific_Interactions/Polarizability' title='Polarizability Source'>polarizability </a> "
      if polarizability > avg_pol

        @property_string += "The #{pol_ref} of #{@name} is #{polarizability.to_s}, larger than the #{average}, suggesting a stronger dispersion force. "
        @property_string += "This also suggests that there are more electrons that are loosely bound, implying this compound has a diffuse electron cloud and a large atomic radii. "
      else
        @property_string += "The #{pol_ref} of #{@name} is #{polarizability.to_s}, smaller than the #{average}, suggesting a weaker dispersion force and more positively charged nucleus. "
        @property_string += "This also suggests that there are fewer electrons but are tightly bound and closer to the nucleus, implying this compound has a smaller and denser electron cloud and a small atomic radii. "
        @property_string += "As a result, #{@name}, is not easily polarized by external electrical fields, but has less electron shielding as a result. \n"
      end
    end
  end


  # Formal charge section for compounds
  def write_formal_charge

    # Conversion to
    formal_charge = @formal_charge.to_i
    @property_string += "It also has a formal charge of #{@formal_charge} which suggests that this compound, #{@name}, "
    if formal_charge == 0
      @property_string += "is neutral and that it is a stable compound and possible in nature, in accordance to <a href='https://chem.libretexts.org/Core/Physical_and_Theoretical_Chemistry/Chemical_Bonding/Lewis_Theory_of_Bonding/Lewis_Theory_of_Bonding' title='Lewis Theory'>Lewis Theory</a>. "
    elsif formal_charge == -1 or formal_charge == 1
      @property_string += "is not neutral but is still possible in nature, in accordance to <a href='https://chem.libretexts.org/Core/Physical_and_Theoretical_Chemistry/Chemical_Bonding/Lewis_Theory_of_Bonding/Lewis_Theory_of_Bonding' title='Lewis Theory'>Lewis Theory</a>. "
    else formal_charge <= -2 or formal_charge >= 2
    @property_string += "is not neutral and suggests it is not possible in nature, in accordance to <a href='https://chem.libretexts.org/Core/Physical_and_Theoretical_Chemistry/Chemical_Bonding/Lewis_Theory_of_Bonding/Lewis_Theory_of_Bonding' title='Lewis Theory'>Lewis Theory</a>. "

    end
  end


  def write_Veber



    veber_ref = "<a href='http://pubs.acs.org/doi/abs/10.1021/jm020017n' title='Veber Rules'>Veber's Rule</a>"

    @property_string += "\n Another rule set that also determines a Drug-Likeness is #{veber_ref}. "
    @property_string += "This rule states that in order to be considered to have high oral bioavailabilty and to be considered Drug-Like it must follow these rules: "

    # Assigned as a cast to integer as it won't work in the if conditional otherwise (Why? I have no clue.)
    # This is also done with the later if statments as well that deal with the other properties.
    rotatable_bonds = @rotatable_bonds.to_i

    # Source for all rules taken from: http://www.notesale.co.uk/more-info/69875/Oral-bioavailability-of-drugs-Lipinski-and-Webers-rules
    # Check for the first law of Verber which states that to have better bioavailability it must have less than 10 rotatable bonds.

    if rotatable_bonds <= 10
      @property_string += "Firstly it must have 10 or less rotatable bonds, to which #{@name} has #{@rotatable_bonds}; therefore passing this rule. "
    elsif @rotatable_bonds.nil?
      @property_string += "Firstly it must have 10 or less rotatable bonds, to which #{@name} has 0 and therefore, does not pass this rule. "
    else
      @property_string += "Firstly it must have 10 or less rotatable bonds, to which #{@name} has #{@rotatable_bonds}; therefore, does not pass this rule. "
    end

    # Check for less than 7 rotatable bonds which increases bioavailability
    if rotatable_bonds < 7
      @property_string += "Since #{@name} has less than 7 rotatable bonds, this implies that it has a much higher oral bioavailability than average. "
    end

    # Second rule of Veber states compound must have  Less  than  12  H  bond  donors  or  acceptors  in  total in order to pass.

    @property_string += "Secondly the total amount of Hydrogen bond donors and Hydrogen bond acceptors must total to less than 12. "
    donors = @donors.to_i
    acceptors = @acceptors.to_i

    if donors.to_i + acceptors.to_i < 12
      total = donors.to_i + acceptors.to_i
      @property_string += "In this case #{@name} passes this rule, as it has a combined total of #{total.to_s} donors and acceptors. "
    else
      @property_string += "In this case #{@name} does not pass this rule, as it has a combined total of #{total.to_s} donors and acceptors. Exceeding the numbers required for this rule. "
    end

    # The third rule states the compound must have a polar  surface  area  of  less  than  140  A  (angstroms).
    @property_string += "Finally the total polar surface area must be less than 140 A (Angstroms). "
    polar_surface_area = @polar_surface_area.to_i
    if polar_surface_area < 140
      @property_string += "The polar surface area of #{@name} is below the required surface area, having a polar surface area of #{@polar_surface_area}. As a result it passes this rule. "
      if polar_surface_area < 90
        @property_string += "Since #{@name} has a polar surface area less than 90, it is small enough to bypass the blood-brain barrier allowing for potential administration within the brain."
      else
        @property_string += "Since #{@name} has a polar surface area less than 140, it is small enough to bypass most tissues membranes in which it is administered."
      end

    else
      @property_string += "The polar surface area of #{name} is above the required surface area, having a polar surface area of #{@polar_surface_area}. As a result it fails this rule. "
      @property_string += "Since #{@name} has a polar surface area greater than 140, it has poor ability to bypass through cell membranes."
    end
    @property_string += "\n"

  end
  def write_Lipinski

    @property_string += "#{@name} passes Lipinski's Rule of Five for Drug-Likeness. Lipinski's rule states that a compound will have: "

    unless @donors.nil?
      @property_string += "no more than 5 hydrogen bond​ ​donors (#{@name} has #{@donors}),"
    else
      @property_string += "no more than 5 hydrogen bond​ ​donors, "
    end
    unless @acceptors.nil?
      @property_string += " no more than 10 hydrogen acceptors (#{@name} has #{@acceptors}), "
    else
      @property_string += " no more than 10 hydrogen acceptors, "
    end
    unless @chem_spider_mol_weight.nil?
      @property_string += "a molecular mass under 500 daltons (#{@name} has a mass as stated above), "
    else
      @property_string+= "a molecular mass under 500 daltons, "
    end
    unless @logP.nil?
      @property_string += "and "
      @property_string += "a logP no greater than 5 (#{@name} has a logP of #{@logP.to_f.round(3)}). "
    else
      @property_string += "and "
      @property_string += "a logP no greater than 5. "
    end
  end


def html
"<html><head>
    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">
    <meta charset=\"UTF-8\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <title>ChemoSummarizer</title>

    <!-- Latest compiled and minified CSS -->
    <link rel=\"stylesheet\" href=\"TemplateHTML_files/bootstrap.css\">

    <!-- Optional theme -->
    <link rel=\"stylesheet\" href=\"TemplateHTML_files/bootstrap-theme.css\">
    <link href=\"TemplateHTML_files/result.css\" rel=\"stylesheet\">
  <script type=\"text/javascript\" id=\"umatrix-ua-spoofer\"> ;(function() {
     try {
         /* https://github.com/gorhill/uMatrix/issues/61#issuecomment-63814351 */
         var navigator = window.navigator;
         var spoofedUserAgent = \"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36\";
         if ( spoofedUserAgent === navigator.userAgent ) {
             return;
         }
         var pos = spoofedUserAgent.indexOf('/');
         var appName = pos === -1 ? '' : spoofedUserAgent.slice(0, pos);
         var appVersion = pos === -1 ? spoofedUserAgent : spoofedUserAgent.slice(pos + 1);
         Object.defineProperty(navigator, 'userAgent', { get: function(){ return spoofedUserAgent; } });
         Object.defineProperty(navigator, 'appName', { get: function(){ return appName; } });
         Object.defineProperty(navigator, 'appVersion', { get: function(){ return appVersion; } });
     } catch (e) {
     }
 })();</script></head>
  <body>

  <nav class=\"navbar navbar-inverse navbar-fixed-top\">
    <div class=\"container\">
      <div class=\"navbar-header\">
        <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
          <span class=\"sr-only\">Toggle navigation</span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
          <span class=\"icon-bar\"></span>
        </button>
        <a class=\"navbar-brand\" href=\"#\">ChemoSummarizer</a>
      </div>
      <div id=\"navbar\" class=\"collapse navbar-collapse\">
        <ul class=\"nav navbar-nav\">
          <li class=\"active\"><a href=\"#home\">Home</a></li>
          <li><a href=\"#about\">About</a></li>
          <li><a href=\"#contact\">Contact</a></li>
					<li><a href=\"http://biosummarizer.wishartlab.com/\">BioSummarizer</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

  <div class=\"container\">

    <div class=\"result\">
      <div class=\"container\"><div class=\"row\"><div class=\"col-md-12\"><div class=\"col-md-6\"><h1 style=\"word-break:break-all\">#{@name}</h1></div><br><br><br><div style = \"float:right; margin-left: 20px\"><div style=\" width: 340px; border:5px solid grey;background-color:rgba(200,200,200,0.1) ;text-align:left;position:relative;margin-right:10em;margin-bottom:3em\"><table id=\"protIDs\" class=\"table\" style=\"width: 320px;\"><img src=\"#{@image_src}\" width=\"100%\"/><tr><th COLSPAN = 2 style =\"background-color: rgba(100,100,100,0.1); text-align:center; word-break:keep-all; white-space:nowrap;\">Identifiers</th></tr><tr><th>IUPAC</th><th style=\"word-break:break-all\">#{@spider_iupac}</th></tr><tr><th>InChI Key</th><th style=\"word-break:break-all\">#{@inchikey}</th></tr><tr><th>InChI</th><th style=\"word-break:break-all\">#{@spider_inchi}</th></tr><tr><th>SMILES</th><th style=\"word-break:break-all\">#{@classysmiles}</th></tr><tr><th colspan=\"2\" style=\"background-color: rgba(100,100,100,0.1); text-align:center; word-break:keep-all; white-space:nowrap;\">Classification</th></tr><tr><th>Kingdom</th><th>#{@kingdom}</th></tr><tr><th>Superclass</th><th>#{@superclass}</th></tr><tr><th>Class</th><th>#{@class}</th></tr><tr><th>Direct Parent</th><th>#{@direct_parent}</th></tr><tr><th colspan=\"2\" style=\"background-color: rgba(100,100,100,0.1); text-align:center; word-break:keep-all; white-space:nowrap;\">Basic Properties</th></tr><tr><th>Atomic Mass:</th><th>#{@chem_spider_mol_weight}</th></tr><tr><th>logP</th><th>#{@logP}</th></tr><tr><th>Polar Surface Area</th><th>#{@polar_surface_area}</th></tr><tr><th colspan=\"2\" style=\"background-color: rgba(100,100,100,0.1); text-align:center; word-break:keep-all; white-space:nowrap;\">Database Accessions</th></tr><tr><th>PubChem:</th><th><a target=\"_blank\" href=\"https://pubchem.ncbi.nlm.nih.gov/compound/#{@pubchem_id}\">#{@pubchem_id}</a></th></tr><tr><th>ChemSpider</th><th><a target=\"_blank\" href=\"http://www.chemspider.com/Chemical-Structure.#{@chemspiderid}.html\">#{@chemspiderid}</a></th></tr><tr><th>ClassyFire</th><th><a target=\"_blank\" href=\"http://classyfire.wishartlab.com/entities/#{@inchikey}\">#{@inchikey}</a></th></tr></tbody></table></div></div><div class=\"container\"><p class=\"text-xs-left\">
 #{@desc_string}</p>
<br><br><div class=\"col-md-6\"><h4 id=\"Basic Properties\"><b> Basic Properties</b></h4></div><br><p class=\"text-xs-left\">#{@property_string}<p></p>

</body></html>"
end