# ChemoSummarizer #

A Gem Integrated in a Sinatra Framework to Create Automated Chemical Summaries.

## License ##

Copyright 2016 Zachary Budinski

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Installation ##

    bundle install

## How To Use it (CLI) ##

    ruby -Ilib ./chemoSummarizer 'InChIKey' '' '' -K
    ruby -Ilib ./chemoSummarizer '' 'Name' '' -N
    ruby -Ilib ./chemoSummarizer '' '' 'InChI' -I

## How To Use it (Start local server) ##

    ruby app.rb

You can also visit chemosummarizer.wishartlab.com