# -*- encoding: utf-8 -*-
require File.expand_path('../lib/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Zachary Budinski"]
  gem.email         = ["zbudinsk@ualberta.ca"]
  gem.description   = %q{Summarization tool that collects and aggregates data from multiple sources}
  gem.summary       = %q{Summarization tool that collects and aggregates data from multiple sources}

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.name          = "chemoSummarizer"
  gem.require_paths = ["lib"]
  gem.version       = ChemoSummarizer::VERSION
  
  gem.add_dependency('bio')
  gem.add_dependency('wikipedia-client')
  gem.add_dependency('synonym_cleaner')
  gem.add_dependency('bio-isoelectric_point')
  gem.add_dependency('savon', '~> 2.5.1')
  gem.add_dependency('nokogiri', '>= 1.6.1')
  gem.add_dependency('require_all', '~> 1.3.1')
  gem.add_dependency('dalli', '~> 2.6.0')
  gem.add_dependency('activesupport', ">= 3.0.0")
  gem.add_dependency('i18n')
  gem.add_dependency('rest-client')
  gem.add_dependency('htmlentities')
  gem.add_dependency('hpricot')
  gem.add_dependency('mechanize', '~> 2.7', '>= 2.7.4')
  gem.add_dependency('parallel')
  gem.add_dependency('httpclient')
	gem.add_dependency('similar_text', '~> 0.0.4')
	gem.add_dependency('capybara')
	gem.add_dependency('poltergeist')
  gem.add_dependency('metbuilder')
  #gem.add_dependancy('crack')
  gem.add_development_dependency('rspec','~> 2.5')
  gem.add_development_dependency('rake')
  gem.add_development_dependency('pry')
  gem.add_dependency('awesome_print')
  gem.add_dependency('guard-rspec')
  gem.add_dependency('crack')
  gem.add_dependency('trollop')
  gem.add_dependency('similarity')
  gem.add_dependency('gsl')
  gem.add_dependency('sqlite3','~> 1.3', '>= 1.3.11')
  gem.add_dependency('similarity-NS')
  gem.add_dependency('kmeans-clusterer')
  gem.add_dependency('scalpel')
  gem.add_dependency('pragmatic_segmenter')
end
