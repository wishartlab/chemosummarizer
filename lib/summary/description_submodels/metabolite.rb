module ChemoSummarizer
  module Summary
      class Metabolite < Summary
          include ChemoSummarizer::Summary
          attr_accessor :description_string

          def initialize(compound)
              @compound = compound
              @description_string = write_metabolite_sentences
          end

          def write_metabolite_sentences
              return nil if is_drug
              references_string = ""
              is_primary = is_primary_metabolite
              
              if is_primary # primary metabolite
                references_string += "#{@compound.identifiers.name} is a primary metabolite."
                references_string += " Primary metabolites are metabolically or physiologically essential metabolites."
                references_string += " They are directly involved in an organism’s growth, development or reproduction."
              elsif !is_primary && @compound.is_metabolite # secondary metabolite
                references_string += "#{@compound.identifiers.name} is a secondary metabolite."
                references_string += " Secondary metabolites are metabolically or physiologically non-essential metabolites that may serve a role as defense or signalling molecules."
                references_string += " In some cases they are simply molecules that arise from the incomplete metabolism of other secondary metabolites."
              end

              # special case for arabidopsis
              is_arabidopsis = false
              for origin in @compound.origins
                if origin.name.include? "Arabidopsis"
                  is_arabidopsis = true
                end
              end

              if is_arabidopsis
                references_string = "#{@compound.identifiers.name} is a secondary metabolite."
                references_string += " Secondary metabolites are metabolically or physiologically non-essential metabolites that may serve a role as defense or signalling molecules."
                references_string += " In some cases they are simply molecules that arise from the incomplete metabolism of other secondary metabolites."
              end
              
              return references_string
          end

          def is_primary_metabolite
            return (@compound.pathways.length() > 0 || @compound.pathbank_pathways.length() > 0)
          end

          def is_drug
            return @compound.identifiers.drugbank_id.present?
          end
      end
  end
end