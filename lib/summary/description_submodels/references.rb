module ChemoSummarizer
    module Summary
        class References < Summary
            include ChemoSummarizer::Summary
            attr_accessor :description_string

            def initialize(compound)
                @compound = compound
                @description_string = write_references_sentence
            end

            def write_references_sentence
                references_string = "Based on a literature review "
                num_references = get_num_references
                if num_references < 3
                    references_string += "very few "
                elsif num_references < 6
                    references_string += "a small amount of "
                else
                    references_string += "a significant number of "
                end
                references_string += "articles have been published on #{@compound.identifiers.name}."
                return references_string
            end

            def get_num_references
                references = @compound.references
                citations = @compound.citations
                unique_refs = []
                
                # get all of the citations
                citations.each do |citation_group|
                    citation_group.each do |citation|
                        if citation.key?(:id)
                            id = citation[:id]
                            unless id.nil? || id.empty? || citation[:db] != "pubmed"
                                unique_refs.push(id)
                            end
                        end
                    end
                end

                references.each do |reference|
                    id = reference.pubmed_id
                    unless id.nil? || id.empty?
                        unique_refs.push(id)
                    end
                end
                return unique_refs.uniq.length
            end
        end
    end
end