module ChemoSummarizer
  class PolySearchCompound
    include ChemoSummarizer
    attr_accessor :name, :data, :num
    def initialize(name)
        @name = name
        get_relations(name)
    end

    def file_to_list(file)
      Dir.chdir(File.dirname(__FILE__))
      file = File.open("polyFiles/#{file}","r")
      file.each_line do |line|
        parse_relation(line)
      end
    end

    def parse_relation(term)
      return self if @data.nil?
      hits = @data["hits"]
      hits.each do |key,value|
        print(value[0].ename)
      #  percent = string_difference_percent(term,)
       # if percent > 50
        #  print (hits["ename"].to_s)
        #end
      end
    end

    def string_difference_percent(a, b)
      longer = [a.size, b.size].max
      same = a.each_char.zip(b.each_char).select { |a,b| a == b }.size
      return (longer - same) / a.size.to_f
    end



    def get_relations(name)
        return self if name.nil?
        success = false
        tries = 0
        while !success and tries < 3
          begin
            url = "http://bioqa.cs.ualberta.ca:2046/polysearch?query="+
                name+"&max_hits=100&databases=medline;pmc;wikipedia"
            encoded_url = URI.encode(url)
            URI.parse(encoded_url)
            encoded_url = encoded_url.gsub("[","%5B").gsub("]","%5D")
            #puts encoded_url
            open(encoded_url) {|f| @data = f.read}
            # create a new polysearch2 compound and push results into it for merging
            # into main compound object returned by data-wrangler
            success = true
            @data = @data.gsub("<html><head><title>PolySearch Thesaurus Search Result</title></head><body><pre>","")
            @data = @data.gsub("</pre></body>", "")
            @data = JSON.parse(@data)
          rescue Exception => e
            $stderr.puts "WARNING #{SOURCE}.get_relations #{e.message} #{e.backtrace}"
            tries += 1
            sleep 2
          end
        end
        self
    end
  end
end



